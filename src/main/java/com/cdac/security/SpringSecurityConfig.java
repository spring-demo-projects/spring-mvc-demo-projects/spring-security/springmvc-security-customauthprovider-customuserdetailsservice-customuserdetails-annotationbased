package com.cdac.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	CustomAuthenticationProvider customAuthenticationProvider;

	/*
	 * Configure Authentication Manager
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 * By default spring comes with 2 Authentication providers :
		 * 
		 * 1. JDBC Authentication Provider
		 * 
		 * 2. In-Memory Authentication Provider
		 */

		/*
		 * Custom Authentication Provider
		 */
		auth.authenticationProvider(getCustomAuthenticationProvider());

	}

	private AuthenticationProvider getCustomAuthenticationProvider() {
		return customAuthenticationProvider;
	}

	private PasswordEncoder getNoPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

}
