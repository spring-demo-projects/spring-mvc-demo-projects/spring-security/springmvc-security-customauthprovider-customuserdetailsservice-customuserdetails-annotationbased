package com.cdac.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	UserDetailsService userDetailsService;

	/*
	 * The authenticate() method receives an unauthenticated Authentication object
	 * as a parameter and returns a fully authenticated Authentication object having
	 * the final authentication result including credentials.
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		/*
		 * authentication => unauthenticated Authentication object
		 * 
		 * By default this object is implementation class :
		 * UsernamePasswordAuthenticationToken
		 */

		/*
		 * Get Username and Password from authentication object (which is
		 * unauthenticated)
		 */
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		/*
		 * Get Customer/User object from database
		 * 
		 * We are retrieving customer object by username
		 * 
		 * This customer object have password data field which we will use to compare
		 * user provided password and database password
		 */
		UserDetails customer = userDetailsService.loadUserByUsername(authentication.getName());

		/*
		 * If no customer present in database with user provided username/email then
		 * throw exception saying "User Does Not Exist"
		 */
		if (customer == null) {
			throw new BadCredentialsException("User Does Not Exist");
		}

		/*
		 * Verify password
		 */
		if (password.equals(customer.getPassword())) {
			/*
			 * Authenticated User
			 * 
			 * Create Authentication Token
			 */
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
					customer.getUsername(), authentication.getCredentials(), customer.getAuthorities());

			System.out.println(authToken);
			/*
			 * Output : org.springframework.security.authentication.
			 * UsernamePasswordAuthenticationToken@8e4b6c12: Principal: admin@cdac.in;
			 * Credentials: [PROTECTED]; Authenticated: true; Details: null; Granted
			 * Authorities: ROLE_ADMIN
			 */
			return authToken;

			/*
			 * If we just update authenticated properties of authentication object to true
			 * then we get below exception
			 * 
			 * java.lang.IllegalArgumentException: Cannot set this token to trusted - use
			 * constructor which takes a GrantedAuthority list instead
			 */
//			authentication.setAuthenticated(true);
//			return authentication;
		} else {
			/*
			 * Non-Authenticated User
			 */
			throw new BadCredentialsException("Incorrect Password");

		}
	}

	/*
	 * decide which kinds of Authentication objects the new AuthenticationProvider
	 * supports? Override the supports() method to specify this. In this example, we
	 * are using UsernamePasswordAuthenticationToken type that supports username and
	 * password based authentication flows.
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
